package manage.ship.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import manage.ship.model.GoodModel;
import manage.ship.service.OperatorService;

public class OperatorServiceImplTest {

	private OperatorService operatorService;

	@Before
	public void initBean() {

		operatorService = new OperatorServiceImpl();
		operatorService.initRatio(2, 3, 5);
	}

	@Test
	public void testSplit() throws Exception {

		int total = 100;

		GoodModel split = operatorService.split(total);

		assertEquals(split.getCurrentQuantity(), total);
		assertEquals(split.getChildGood()[0].getCurrentQuantity(), 20);
		assertEquals(split.getChildGood()[1].getCurrentQuantity(), 30);
		assertEquals(split.getChildGood()[2].getCurrentQuantity(), 50);
	}

	@Test
	public void testIncreaseSplit() throws Exception {

		int total = 200;

		GoodModel split = operatorService.split(total);

		assertEquals(split.getCurrentQuantity(), total);
		assertEquals(split.getChildGood()[0].getCurrentQuantity(), 40);
		assertEquals(split.getChildGood()[1].getCurrentQuantity(), 60);
		assertEquals(split.getChildGood()[2].getCurrentQuantity(), 100);
	}

	@Test
	public void testDecreaseSplit() throws Exception {

		int total = 50;

		GoodModel split = operatorService.split(total);

		assertEquals(split.getCurrentQuantity(), total);
		assertEquals(split.getChildGood()[0].getCurrentQuantity(), 10);
		assertEquals(split.getChildGood()[1].getCurrentQuantity(), 15);
		assertEquals(split.getChildGood()[2].getCurrentQuantity(), 25);
	}

	@Test
	public void testMerge() throws Exception {

		int total = 100;

		GoodModel split = operatorService.merge(total);
		int merge = split.merge(split.getChildGood()[0], split.getChildGood()[1]);
		assertEquals(merge, 80);
	}

}
