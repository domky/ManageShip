package manage.ship.service;

import manage.ship.model.GoodModel;

public interface OperatorService {

	void initRatio(int... ration);
	
	GoodModel split(int total);
	GoodModel merge(int total);
}
