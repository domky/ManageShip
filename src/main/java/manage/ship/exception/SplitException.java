package manage.ship.exception;

public class SplitException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public SplitException(String msg) {
		super(msg);
	}
}
