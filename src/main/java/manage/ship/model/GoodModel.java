package manage.ship.model;

import java.util.Arrays;
import java.util.Comparator;

import com.alibaba.fastjson.JSON;

import lombok.Data;

@Data
public class GoodModel {

	private int currentQuantity;

	private GoodModel[] childGood;

	public int merge(GoodModel gm1, GoodModel gm2) {
		return gm1.getCurrentQuantity() + gm2.getCurrentQuantity();
	}

	public GoodModel sort() {
		Arrays.sort(childGood, new Comparator<GoodModel>() {
			@Override
			public int compare(GoodModel g1, GoodModel g2) {
				if (g1.getCurrentQuantity() < g2.getCurrentQuantity()) {
					return 1;
				} else if (g1.getCurrentQuantity() > g2.getCurrentQuantity()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return this;
	}

	@Override
	public String toString() {
		if (childGood == null) {
			return "currentQuantity：" + currentQuantity;
		} else {
			return "currentQuantity：" + currentQuantity + " childGood:" + JSON.toJSONString(childGood);
		}
	}

}
