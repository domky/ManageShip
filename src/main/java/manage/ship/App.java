package manage.ship;

import spring.module.SpringApplication;
import spring.module.SpringBootApplication;

@SpringBootApplication
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
