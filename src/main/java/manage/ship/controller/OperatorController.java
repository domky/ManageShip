package manage.ship.controller;

import java.util.HashMap;
import java.util.Map;

import manage.ship.model.GoodModel;
import manage.ship.service.OperatorService;
import spring.module.Autowired;
import spring.module.PathVariable;
import spring.module.RequestMapping;
import spring.module.RequestMethod;
import spring.module.RestController;

@RestController
public class OperatorController {

	@Autowired
	private OperatorService operatorService;
	
	@RequestMapping(value = "/initRatio", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public void initRatio(int[] ration) {
		
		operatorService.initRatio(ration);
	}

	@RequestMapping(value = "/merge/{total}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public Map<String, Object> merge(@PathVariable("total") Integer total) {

		GoodModel split = operatorService.merge(total);
		int merge = split.merge(split.getChildGood()[0], split.getChildGood()[1]);
		Map<String, Object> result = new HashMap<String, Object>() {
			private static final long serialVersionUID = 1L;

			{
				put("gm1", split.getChildGood()[0]);
				put("gm2", split.getChildGood()[1]);
				put("merge", merge);
			}
		};
		return result;
	}

	@RequestMapping(value = "/increaseSplit/{total}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public GoodModel increaseSplit(@PathVariable("total") Integer total) {
		return operatorService.split(total);
	}

	@RequestMapping(value = "/decreaseSplit/{total}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public GoodModel decreaseSplit(@PathVariable("total") Integer total) {
		return operatorService.split(total);
	}

	@RequestMapping(value = "/split/{total}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public GoodModel split(@PathVariable("total") Integer total) {
		return operatorService.split(total);
	}
}
